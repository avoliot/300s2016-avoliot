from Bio.Blast import NCBIWWW
#help(NCBIWWW.qblast)
from Bio import SeqIO
from Bio.Blast import NCBIXML
import re

def doBlast(fileName, matrix):
    record = SeqIO.read(fileName, format="fasta")
    result_handle = NCBIWWW.qblast("blastp", "nr", record.seq, matrix_name=matrix)
    save_file = open(fileName + "Blast"+str(matrix)+".xml", "w")
    save_file.write(result_handle.read())
    save_file.close()
    result_handle.close() 

def getNames(fileName):
    result_handle = open(fileName)
    blast_record = NCBIXML.read(result_handle)
    patt = re.compile("(\[.+\])")     
    lstOfNames = []
    #print '#'*30, "\n Names of all sequences:\n",'#'*30,'\n'
    for i,alignment in enumerate(blast_record.alignments):
        mo = patt.search(str(alignment.title)[0:100])
        if mo:
            name = mo.groups()[0]
            #print i+1, name
            lstOfNames.append(name)
    result_handle.close()        
    return lstOfNames        

def printFreq(lst62,lst90):
    allHits = lst62+lst90
    lst_nodups = list(set(allHits))
    dict62 = {}
    dict90 = {}
    for name in lst62:
        if name in lst62 and name in dict62:
            dict62[name] =  dict62[name] + 1
        else:
            dict62[name] =  1
    for name in lst90:   
        if name in lst90 and name in dict90:
            dict90[name] =  dict90[name] + 1
        else:
            dict90[name] =  1   
    print "Name"+" "*55+"Blosum62"+" "*15+"Blosum90"
    print '#'*90, '\n'
    for name in lst_nodups:
        print( "%-40s ==> %20d %20d" %(name,dict62[name],dict90[name]) ) 
  

def main():
    seqFile = "ConservedAAFomEColiAndOtherGramNegOrg.txt";   
    doBlast(seqFile, "BLOSUM62") #default
    doBlast(seqFile, "BLOSUM90") #closely related org.   

    seqFile = "ConservedAAFomEColiAndOtherGramNegOrg.txtBlastBLOSUM62.xml";  
    blosum62 = getNames(seqFile)

    seqFile = "ConservedAAFomEColiAndOtherGramNegOrg.txtBlastBLOSUM90.xml";  
    blosum90 = getNames(seqFile)

    print( "%-50s %-50s" %("BLOSUM62","BLOSUM90") )
    for name62, name90 in zip(blosum62, blosum90): # zip() combines multiple lists together, for example if you zip() three lists containing 20 elements each, the result has twenty elements, but each element is a three-tuple.
        print( "%-50s %-50s" %(name62,name90) )

    printFreq(blosum62,blosum90)

main()
