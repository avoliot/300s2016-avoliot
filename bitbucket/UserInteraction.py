# Ted Avolio
# User interaction and printing

person = raw_input('Enter your name: ')
print 'Hello', person

# age is a variable of type string
age = raw_input('What is your age? ')
age = int(age) # age is a variable of type int now
age = age + 2
print "In two years you will be ", age, " years old."

age1= input("Enter a number: ")
age1= age1+2
print "New age is ", age1
